import csv
import os
import tarfile
from rdflib import Graph
from libraries.image_classification.constants import GoldStandardConstants as gsc
from libraries.image_classification.utils import Constants, DatabaseFunctions, PrintHelper
from  libraries.text_classification.backend.utils.constants import GOLD_STANDARDS_DIR as gold_standards_folder, \
    ALL_GOLD_STANDARDS_ZIP_DIR as agZip


class DocumentParser:
    def __str__(self):
        return 'DocumentParser'

    def __init__(self, document_name, document_language, document_delimiter, document_encoding, document_description,
                 documents, document_text_array=None):

        self._document_temp_name = document_name + "_temp_file"
        self._document_save_name = document_name
        self._document_language = document_language
        self._document_delimiter = document_delimiter
        self._document_encoding = document_encoding
        self._document_description = document_description
        self._document_files = documents
        self._document_text_array = []
        if document_text_array is not None:
            self._document_text_array = document_text_array
        self._gold_standards_folder = gold_standards_folder + '/'
        self._document_directory = self._gold_standards_folder + document_name + '/'
        self._file_extension = self._get_document_type()
        self._check_and_add_folders()
        pass

    def _get_document_type(self):
        try:
            if len(self._document_text_array) > 0:
                return ".array"
            else:
                file_name, file_extension = os.path.splitext(self._document_files)
                return file_extension
        except Exception as e:
            PrintHelper.failure_print("Error _get_document_type:  ", e)
            return '.txt'

    def _parse_txt_to_csv(self):
        try:
            self._created_temp_file_name = self._document_temp_name + '.csv'
            self._created_file_name = self._document_save_name + '.csv'

            txt_file = self._document_directory + self._document_save_name + self._file_extension
            csv_file = self._document_directory + self._created_temp_file_name

            in_txt = csv.reader(open(txt_file, "rt", encoding=self._document_encoding),
                                delimiter=self._document_delimiter)
            with open(csv_file, 'w+', encoding=self._document_encoding) as f:
                out_csv = csv.writer(f)
                out_csv.writerows(in_txt)

            self._insert_csv_quotes()
            self._update_created_file_name()
            pass

        except Exception as e:
            PrintHelper.failure_print("Error in _parse_txt_to_csv:  ", e)
            pass

    def _parse_rdf_to_csv(self):

        try:
            file_graph = Graph()
            format = self._file_extension.replace(".", "")
            file_graph.load(self._document_directory + self._document_save_name + self._file_extension, format=format)
            self._created_temp_file_name = self._document_temp_name + '.csv'
            self._created_file_name = self._document_save_name + '.csv'
            with open(self._document_directory + self._created_temp_file_name, 'wt',
                      encoding=self._document_encoding) as csv_file:
                for subject, predicate, object in file_graph:
                    csv_file_writer = csv.writer(csv_file)
                    csv_file_writer.writerow([subject, predicate, object])

            self._insert_csv_quotes()
            self._update_created_file_name()

        except Exception as e:
            PrintHelper.failure_print("Error in _parse_rdf_to_csv:  ", e)
            pass

    def _update_created_file_name(self):
        try:
            mongo_item = DatabaseFunctions().find_item(gsc.database, gsc.collection, gsc.gs_name,
                                                       self._document_save_name)
            update_item = {
                gsc.gs_created_file_name: self._created_file_name
            }
            DatabaseFunctions().update_items(gsc.database, gsc.collection, mongo_item["_id"], update_item)
        except Exception as e:
            PrintHelper.failure_print("Error in _update_created_file_name:  ", e)

    def _check_for_valid_name(self):
        mongo_item = DatabaseFunctions().find_item(gsc.database, gsc.collection, gsc.gs_name, self._document_save_name)
        if mongo_item == None:
            return True
        else:
            return False

    def _insert_csv_quotes(self):
        try:
            temp_csv_file = open(self._document_directory + self._created_temp_file_name, "r+", encoding="iso-8859-1")
            final_csv_file = csv.writer(
                    open(self._document_directory + self._created_file_name, "w+", encoding=self._document_encoding))
            csv_reader = csv.reader(temp_csv_file)
            for rows in list(csv_reader):
                processed_element = []
                for elements in rows:
                    processed_element.append("\"" + elements + "\"")
                final_csv_file.writerow(processed_element)

            os.remove(self._document_directory + self._created_temp_file_name)
        except Exception as e:
            PrintHelper.failure_print("Error in _insert_csv_quotes:  ", e)
            pass

    def _save_annotated_dataset_from_array(self):
        try:
            Constants.directory_validation(self._document_directory)
            self._created_temp_file_name = self._document_temp_name + '.csv'
            self._created_file_name = self._document_save_name + '.csv'

            description_file = open(self._document_directory + 'description.txt', "w+")
            description_file.write(self._document_description)
            description_file.close()

            language_file = open(self._document_directory + 'language.txt', "w+")
            language_file.write(self._document_language)
            language_file.close()

            csv_file = self._document_directory + self._created_temp_file_name

            with open(csv_file, 'w+', encoding=self._document_encoding) as f:
                out_csv = csv.writer(f)
                for text in self._document_text_array:
                    lines = text.split(",")
                    out_csv.writerow([lines[0], lines[1]])

            self._insert_csv_quotes()
            self._update_created_file_name()
        except Exception as e:
            PrintHelper.failure_print("Error in _save_annotated_dataset_from_array:  ", e)
            pass

    def check_document_validity(self):

        if not self._check_for_valid_name():
            return (False,
                    'Another document with the same name already exists. Please choose a different name or version number')

        return (True, 'Success')

    def save_document(self, document, extension):
        try:
            Constants.directory_validation(self._document_directory)
            file_name = self._document_directory + self._document_temp_name + extension
            description_file = open(self._document_directory + 'description.txt', "w+")
            description_file.write(self._document_description)
            description_file.close()

            language_file = open(self._document_directory + 'language.txt', "w+")
            language_file.write(self._document_language)
            language_file.close()

            document.save(file_name)
            mongo_items = {
                gsc.gs_name              : self._document_save_name,
                gsc.gs_original_file_name: self._document_save_name + self._file_extension,
                gsc.gs_description       : self._document_description,
                gsc.gs_language          : self._document_language,
                gsc.gs_created_file_name : ''
            }
            DatabaseFunctions().insert_item(gsc.database, gsc.collection, mongo_items)
            return file_name

        except Exception as e:
            PrintHelper.failure_print("Error in _save_document:  ", e)
            return False

    def parse_document(self):

        if self._file_extension == '.csv':
            self._insert_csv_quotes()
            pass
        elif self._file_extension == '.txt':
            self._parse_txt_to_csv()
        elif self._file_extension == '.ttl' or self._file_extension == '.nt' or self._file_extension == '.nb' or self._file_extension == '.xml' or self._file_extension == '.trix' or self._file_extension == '.rdfa':
            self._parse_rdf_to_csv()
        elif self._file_extension == '.array':
            mongo_items = {
                gsc.gs_name              : self._document_save_name,
                gsc.gs_original_file_name: self._document_save_name + self._file_extension,
                gsc.gs_description       : self._document_description,
                gsc.gs_language          : self._document_language,
                gsc.gs_created_file_name : ''
            }
            DatabaseFunctions().insert_item(gsc.database, gsc.collection, mongo_items)
            self._save_annotated_dataset_from_array()
        else:
            pass

    def _check_and_add_folders(self):
        try:
            Constants.directory_validation(self._gold_standards_folder)
            list_of_folders = os.listdir(self._gold_standards_folder)
            db = DatabaseFunctions()
            database_items = db.find_all_items(gsc.database, gsc.collection)
            if database_items is not None and len(database_items) is not len(list_of_folders):
                reload = True
            elif database_items is None:
                reload = True
            else:
                reload = False

            if reload:
                PrintHelper.info_print("deleted")
                db.delete_database(gsc.database)
                with tarfile.open(agZip + "/all_gold_standards.tar.bz2", "w:bz2") as compressed_file:
                    compressed_file.add(gold_standards_folder, arcname="all_gold_standards.tar.bz2")

                for directory in list_of_folders:
                    directory_path = self._gold_standards_folder + directory + '/'
                    description = ''
                    language = ''
                    if os.path.isdir(directory_path) == True:
                        files_in_folder = os.listdir(directory_path)
                        for files in files_in_folder:
                            if files == 'description.txt':
                                description_file = open(directory_path + 'description.txt', encoding='utf-8')
                                description = description_file.read()
                            if files == 'language.txt':
                                language_file = open(directory_path + 'language.txt', encoding='utf-8')
                                language = language_file.read()

                        mongo_items = {
                            gsc.gs_name       : directory,
                            gsc.gs_description: description,
                            gsc.gs_language   : language,
                        }
                        db.insert_item(gsc.database, gsc.collection, mongo_items)

                    else:
                        os.remove(self._gold_standards_folder + directory)
        except Exception as e:
            PrintHelper.failure_print("check_and_add_folders    ", str(e))
            pass
