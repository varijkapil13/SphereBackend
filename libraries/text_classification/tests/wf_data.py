workflow_data_2 = {
    "nodes": [
        {
            "id": "1498060196917",
            "name": "lorenz_SA_en_GitHub_speech_act",
            "type": "gs",
            "color": "blue"
        },
        {
            "id": "1498060202557",
            "name": "tf_idf",
            "type": "fe",
            "color": "orange"
        },
        {
            "id": "1498060215584",
            "name": "svm",
            "type": "cf",
            "color": "red"
        }
    ],
    "links": [
        {
            "id": "1498060218971",
            "source": "1498060196917",
            "target": "1498060202557"
        },
        {
            "id": "1498060224067",
            "source": "1498060202557",
            "target": "1498060215584"
        }
    ],
    "workflow_name": "emotions-workflow",
    "workflow": [
        {
            "group": "nodes",
            "data": {
                "id": "1498060196917",
                "name": "emotions",
                "type": "gs",
                "color": "blue"
            },
            "position": {
                "x": 407.56801722673276,
                "y": 247.57992525870768
            }
        },
        {
            "group": "nodes",
            "data": {
                "id": "1498060202557",
                "name": "pretrainedWord2vec",
                "type": "fe",
                "color": "orange"
            },
            "position": {
                "x": 566.1677680890917,
                "y": 163.87450119246267
            }
        },
        {
            "group": "nodes",
            "data": {
                "id": "1498060205110",
                "name": "tf_idf",
                "type": "fe",
                "color": "orange"
            },
            "position": {
                "x": 567.0488778161048,
                "y": 253.74769334779938
            }
        },
        {
            "group": "nodes",
            "data": {
                "id": "1498060210284",
                "name": "sentiment",
                "type": "fe",
                "color": "orange"
            },
            "position": {
                "x": 561.7622194540262,
                "y": 373.57861622158174
            }
        },
        {
            "group": "nodes",
            "data": {
                "id": "1498060215584",
                "name": "multi_naive_bayes",
                "type": "cf",
                "color": "red"
            },
            "position": {
                "x": 819.0462597418531,
                "y": 259.0343517098781
            }
        },
        {
            "group": "edges",
            "data": {
                "id": "1498060218971",
                "source": "1498060196917",
                "target": "1498060202557"
            }
        },
        {
            "group": "edges",
            "data": {
                "id": "1498060220219",
                "source": "1498060196917",
                "target": "1498060205110"
            }
        },
        {
            "group": "edges",
            "data": {
                "id": "1498060221682",
                "source": "1498060196917",
                "target": "1498060210284"
            }
        },
        {
            "group": "edges",
            "data": {
                "id": "1498060222841",
                "source": "1498060205110",
                "target": "1498060215584"
            }
        },
        {
            "group": "edges",
            "data": {
                "id": "1498060224067",
                "source": "1498060202557",
                "target": "1498060215584"
            }
        },
        {
            "group": "edges",
            "data": {
                "id": "1498060225529",
                "source": "1498060210284",
                "target": "1498060215584"
            }
        }
    ]
}
