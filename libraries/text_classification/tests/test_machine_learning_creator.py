import unittest

import peewee
import requests

from  libraries.text_classification.backend.mysql.utils.secret import MY_SQL_SECRET_PW
from  libraries.text_classification.backend.wrapper import ResourceWrapper
from  libraries.text_classification.classification.machine_learning_creator import \
    GenericMLclassifierCreator
from  libraries.text_classification.tests.wf_data import workflow_data_2


def rest_call(path, method, payload=None):
    base_url = "http://localhost:5000/"

    if method == "POST":
        headers = {'Content-Type': 'application/json'}
        response = requests.post(base_url + path, json=payload, headers=headers)
    if method == "GET":
        response = requests.get(base_url + path)
    return response


class MachineLearningCreatorTestCase(unittest.TestCase):
    def setUp(self):
        # RESET DATABASES
        # path = "api/db_reset"
        # rest_call(path=path, method="GET")
        pass

    def tearDown(self):
        # Delete created classifier
        # for filename in os.listdir(CLASSIFIERS_DIR):
        #     if os.path.isfile(os.path.join(CLASSIFIERS_DIR, filename)):
        #         os.remove(os.path.join(CLASSIFIERS_DIR, filename))
        pass

    def test_create_classifier(self):
        # GIVEN
        path = "sphere/api/v1/resources/workflows"
        payload = workflow_data_2

        # WHEN
        response = rest_call(path=path, method="POST", payload=payload)

        # THEN
        try:
            self.assertEqual(200, response.status_code)
        except AssertionError:
            # error occured
            print(response.text)

    def test_prediction_api(self):
        # GIVEN
        database = peewee.MySQLDatabase("uby", host="127.0.0.1", port=3306,
                                        user="root",
                                        passwd=MY_SQL_SECRET_PW)
        resource_wrapper = ResourceWrapper(database)
        classifier = GenericMLclassifierCreator(workflow_data=workflow_data_2,
                                                resource_wrapper=resource_wrapper)
        classifier.start_building_classifier()
        classifier_id = classifier.get_classifier_id()
        path = "sphere/api/v1/workflow/" + str(classifier_id) + "/predict?text=how%are%you%today"

        # THEN
        response = rest_call(path=path, method="GET")
        self.assertEqual(200, response.status_code)

    def test_get_resource_api(self):
        # GIVEN
        path = "sphere/api/v1/resources"
        path2 = "sphere/api/v1/resources/goldstandards"
        path3 = "sphere/api/v1/resources/extractors"
        path4 = "sphere/api/v1/resources/classifiers"
        path5 = "sphere/api/v1/resources/workflows"
        path6 = "sphere/api/v1/resources/workflows?searchstring=Emoo"

        # THEN
        response_1 = rest_call(path=path, method="GET")
        response_2 = rest_call(path=path2, method="GET")
        response_3 = rest_call(path=path3, method="GET")
        response_4 = rest_call(path=path4, method="GET")
        response_5 = rest_call(path=path5, method="GET")
        response_6 = rest_call(path=path6, method="GET")

        self.assertEqual(200, response_1.status_code)
        self.assertEqual(200, response_2.status_code)
        self.assertEqual(200, response_3.status_code)
        self.assertEqual(200, response_4.status_code)
        self.assertEqual(200, response_5.status_code)
        self.assertEqual(200, response_6.status_code)

    def test_deep_learning_creator(self):
        # GIVEN
        workflow_data_2["nodes"][2]["name"] = 'keras'
        self.train(workflow_data_2)

    def test_upload_lexica(self):
        path = "sphere/api/v1/addLexicon"
        import json
        with open('sample.json') as data_file:
            data = json.load(data_file)

        response = rest_call(path, method="POST", payload=data)
        self.assertEqual(200, response.status_code)

    def test_all_feature_extractor(self):
        # workflow_data_2["nodes"][1]["name"] = 'tf_idf'
        # self.train(workflow_data_2)
        # workflow_data_2["nodes"][1]["name"] = 'word2vec_affectivespace'
        # self.train(workflow_data_2)
        # workflow_data_2["nodes"][1]["name"] = 'sentiment_sentiwordnet'
        # self.train(workflow_data_2)
        # workflow_data_2["nodes"][1]["name"] = 'word2vec_affectivespace_tf_idf'
        # self.train(workflow_data_2)
        workflow_data_2["nodes"][1]["name"] = 'hypernym'
        self.train(workflow_data_2)
        workflow_data_2["nodes"][1]["name"] = 'hyponym'
        self.train(workflow_data_2)
        workflow_data_2["nodes"][1]["name"] = 'pos_tag'
        self.train(workflow_data_2)
        workflow_data_2["nodes"][1]["name"] = 'gen_inq'
        self.train(workflow_data_2)

    def train(self, workflow):
        database = peewee.MySQLDatabase("uby", host="127.0.0.1", port=3306,
                                        user="root",
                                        passwd=MY_SQL_SECRET_PW)
        resource_wrapper = ResourceWrapper(database)

        classifier = GenericMLclassifierCreator(workflow_data=workflow,
                                                resource_wrapper=resource_wrapper)
        classifier.start_building_classifier()


if __name__ == "__main__":
    import logging

    logger = logging.getLogger('peewee')
    logger.setLevel(logging.DEBUG)
    logger.addHandler(logging.StreamHandler())

    suite = unittest.TestSuite()
    # suite.addTest(MachineLearningCreatorTestCase("test_create_classifier"))
    # suite.addTest(MachineLearningCreatorTestCase("test_prediction_api"))
    # suite.addTest(MachineLearningCreatorTestCase("test_get_resource_api"))
    # suite.addTest(MachineLearningCreatorTestCase("test_deep_learning_creator"))
    # suite.addTest(MachineLearningCreatorTestCase("test_upload_lexica"))
    # suite.addTest(MachineLearningCreatorTestCase("test_all_feature_extractor"))
    runner = unittest.TextTestRunner()
    runner.run(suite)
