import json
import os

import re
from pymongo import MongoClient
from io import open
from libraries.text_classification.backend.utils.constants import LEXICON_DATA_DIR
from libraries.image_classification.utils import Constants

class MongoLexiconDatabaseHandler(object):
    """

    """

    def __init__(self, lexicon_name):
        self._lexicon_name = lexicon_name
        self._client = MongoClient(Constants.mongoDB['url'], Constants.mongoDB['port'])
        self._lexicon_db = self._client.LexiconDatabase
        self._retrieve_dataset_collection()

    def _retrieve_dataset_collection(self):
        collectionName = "collection_for_" + self._lexicon_name
        self.lexicon_collection = self._lexicon_db[collectionName]

        if self.lexicon_collection.count() == 0:
            self._insert_documents_from_upload(self._lexicon_name)

    def _insert_documents_from_upload(self, filename):

        documents = []
        with open(os.path.join(LEXICON_DATA_DIR, filename + ".json"), "r", newline='',
                  encoding="utf-8") as jsonfile:
            entries = json.load(jsonfile)

        jsonfile.close()

        for entry in entries:
            entry["lexicon_name"] = self._lexicon_name
            documents.append(entry.copy())

        self._write_into_dataset_collection(documents)

        return self.lexicon_collection.name

    def _write_into_dataset_collection(self, documents):
        self.lexicon_collection.insert_many(documents)

    def get_all_docs_from_lexicon(self):
        allDocs = []
        cursor = self.lexicon_collection.find({}, {"lexicon_name": 1, "words": 1, "polarity": 1,
                                                   "gloss": 1, "_id": 0})

        for doc in cursor:
            allDocs.append(doc)
        return allDocs

    def get_doc_from_lexicon(self, word):
        regex = re.compile(word, re.IGNORECASE)
        cursor = self.lexicon_collection.find({"words": regex}, {"_id": 0})

        docs = [doc for doc in cursor]
        # for doc in docs:
        #     doc["_id"] = str(doc["_id"])

        return docs

    def clear_dataset_collection(self):
        self.lexicon_collection.remove({})

    def print_collection(self):
        cursor = self.lexicon_collection.find({})
        for doc in cursor:
            print(doc)

