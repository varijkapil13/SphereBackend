import csv
import os

from io import open
from pymongo import MongoClient
from app.libraries.text_classification.backend.utils.constants import DATA_DIR
from app.libraries.image_classification.utils import Constants


class MongoAPIDatabaseHandler(object):
    def __init__(self):
        self._client = MongoClient(Constants.mongoDB['url'], Constants.mongoDB['port'])
        self.api_database = self._client.APIDatabase
        self.collection = self.api_database["all_resources_collection"]

        self.clear_resource_collection()
        self._insert_docs_from_csv_file()

    def _insert_docs_from_csv_file(self):
        document = {
            "name": None,
            "type": None
        }
        documents = []
        path = os.path.join(DATA_DIR, "all_resources.csv")
        with open(path, "r", newline='', encoding="utf-8") as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                document["name"] = row[0]
                document["type"] = row[1]
                documents.append(document.copy())

        csvfile.close()
        self.collection.insert_many(documents)

    def get_classifier_types(self):
        cursor = self.collection.find({"type": "classifier"}, {"name": 1, "type": 1, "_id": 0})
        classifierTypes = [doc for doc in cursor]

        return classifierTypes

    def get_gold_standard_types(self):
        cursor = self.collection.find({"type": "gold_standard"}, {"name": 1, "type": 1, "_id": 0})
        goldStandardTypes = [doc for doc in cursor]

        return goldStandardTypes

    def get_feature_extraction_types(self):
        cursor = self.collection.find({"type": "feature_extraction"},
                                      {"name": 1, "type": 1, "_id": 0})
        featureExtractionTypes = [doc for doc in cursor]

        return featureExtractionTypes

    def get_all_resources(self):
        cursor = self.collection.find({}, {"type": 1, "name": 1, "_id": 0})
        resources = [doc for doc in cursor]

        return resources

    def clear_resource_collection(self):
        self.collection.remove({})

    def print_collection(self):
        cursor = self.collection.find({})
        for doc in cursor:
            print(doc)

    def delete_database(self):
        self._client.drop_database("APIDatabase")

if __name__ == "__main__":
    new = MongoAPIDatabaseHandler()
