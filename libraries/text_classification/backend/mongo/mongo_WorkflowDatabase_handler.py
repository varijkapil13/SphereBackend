import re

from pymongo import MongoClient
from app.librariesimage_classification.utils import Constants


class MongoWorkflowDatabaseHandler(object):
    def __init__(self):
        self._client = MongoClient(Constants.mongoDB['url'], Constants.mongoDB['port'])
        self._workflowDb = self._client.WorkflowDatabase
        self._workflow_collection = self._workflowDb["workflow_collection"]

    def insert_workflow_data(self, workflow, scores):
        document = {
            "name"    : workflow["workflow_name"],
            "status"  : "trained",
            "workflow": workflow["workflow"],
            "scores"  : scores
        }
        inserted_object = self._workflow_collection.insert_one(document)
        return inserted_object.inserted_id

    def get_all_workflows(self):
        cursor = self._workflow_collection.find({})
        return [doc for doc in cursor]

    def get_workflows(self, searchstring):
        regex = re.compile(searchstring, re.IGNORECASE)
        cursor = self._workflow_collection.find({"name": regex})

        return [doc for doc in cursor]

    def print_collection(self):
        cursor = self._workflow_collection.find({})
        for doc in cursor:
            print(doc)

    def delete_database(self):
        self._client.drop_database("WorkflowDatabase")