import csv
import os
import numpy as np
import spacy

from pymongo import MongoClient
from io import open
from app.libraries.text_classification.backend.utils.constants import DATA_DIR, TRAINING_DATA_DIR, \
    GOLD_STANDARDS_DIR
from app.libraries.image_classification.utils import Constants


class MongoDatasetDatabaseHandler(object):
    """
        Handles the basic datasets for emotion sentiment gender ...

        Attributes:
           _dataset_name: The name of the dataset that will be saved
           _client: MongoClient
           _dataset_db: Mongo database for text classification
           dataset_collection: Mongo Collection. 
           documents: a list of dictionaries which will be inserted into the collection
    """

    # SActA_en_UCIMLRepo_SentenceLabellingWordList#StopWords_v1
    def __init__(self, datasetname=None):
        self._client = MongoClient(Constants.mongoDB['url'], Constants.mongoDB['port'])
        self._dataset_db = self._client.DatasetDatabase

        if datasetname is not None:
            self._dataset_name = datasetname
            self._retrieve_dataset_collection()

    def _write_into_dataset_collection(self, documents):
        self.dataset_collection.insert_many(documents)

    def _retrieve_dataset_collection(self):
        collectionName = "collection_for_" + self._dataset_name
        self.dataset_collection = self._dataset_db[collectionName]

        if self.dataset_collection.count() == 0:
            self._insert_gold_standard_from_upload(self._dataset_name)

    def _insert_gold_standard_from_upload(self, foldername):
        document = {
            "dataset_name": self._dataset_name,
            "sample"      : None,
            "labels"      : None
        }
        documents = []
        with open(os.path.join(GOLD_STANDARDS_DIR, foldername, foldername + ".csv"), "r",
                  newline='',
                  encoding="utf-8") as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                try:
                    document["sample"] = row[0]
                    document["labels"] = [label for label in row[1:]]
                    documents.append(document.copy())
                except:
                    continue

        csvfile.close()

        self._write_into_dataset_collection(documents)

        return self.dataset_collection.name

    def print_collection(self):
        cursor = self.dataset_collection.find({})
        for doc in cursor:
            print(doc)

    def get_all_docs_from_dataset_collection(self):
        allDocs = []
        cursor = self.dataset_collection.find({}, {"sample": 1, "label": 1, "_id": 0})

        for doc in cursor:
            allDocs.append(doc)
        return allDocs

    def get_samples_from_all_docs_tokenized(self, asSpaCyTokens):
        cursor = self.dataset_collection.find({}, {"sample": 1, "_id": 0})
        samples = [document["sample"] for document in cursor]

        # tokenize data with spaCy
        nlp = spacy.load("en")
        data = []
        for sample in samples:
            tokens = []
            for token in nlp(sample):
                if asSpaCyTokens:
                    tokens.append(token)
                else:
                    tokens.append(token.text)
            data.append(tokens)

        return np.array(data)

    def get_samples_from_all_docs(self):
        cursor = self.dataset_collection.find({}, {"sample": 1, "_id": 0})
        samples = [document["sample"] for document in cursor]

        return np.array(samples)

    def get_labels_from_all_docs(self):
        is_multi_label = False

        cursor = self.dataset_collection.find({}, {"labels": 1, "_id": 0})
        mongo_label_fields = [document["labels"] for document in cursor]

        for label in mongo_label_fields:
            if len(label) > 1:
                is_multi_label = True

        return np.array(mongo_label_fields), is_multi_label

    def clear_dataset_collection(self):
        self.dataset_collection.remove({})

    def delete_database(self):
        self._client.drop_database("DatasetDatabase")
