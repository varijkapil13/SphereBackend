from pymongo import MongoClient
from app.librariesimage_classification.utils import Constants

class MongoUserDatabase():
    # FOR LATER IMPLEMENTATION --> WHEN USER UPLOADS DATA WE PROBABLY GONNA SAVE IT IN MONGO

    def __init__(self):
        self.client = MongoClient(Constants.mongoDB['url'], Constants.mongoDB['port'])
        self.textDb = self.client.UserDatabase
        pass
