import json
import os
from pprint import pprint

from pymongo import MongoClient

from app.libraries.text_classification.backend.utils.constants import UPLOADED_RESOURCES


class MongoUploadHandler(object):
    def __init__(self, datasetname=None):
        self._client = MongoClient()
        self._dataset_db = self._client.SomeDatabase

        if datasetname is not None:
            self._dataset_name = datasetname
            self._retrieve_dataset_collection()

    def _write_into_dataset_collection(self, documents):
        self.dataset_collection.insert_many(documents)

    def _retrieve_dataset_collection(self):
        collectionName = "collection_for_" + self._dataset_name
        self.dataset_collection = self._dataset_db[collectionName]

        if self.dataset_collection.count() == 0:
            self._insert_gold_standard_from_upload(self._dataset_name)

    def _insert_gold_standard_from_upload(self, foldername):

        with open(os.path.join(UPLOADED_RESOURCES, self._dataset_name + ".json"), "r") as jsonfile:
            data = json.load(jsonfile)

        self._write_into_dataset_collection(data)

        return self.dataset_collection.name

    def print_collection(self):
        cursor = self.dataset_collection.find({})
        for doc in cursor:
            pprint(doc)

    def clear_dataset_collection(self):
        self.dataset_collection.remove({})

    def delete_database(self):
        self._client.drop_database("SomeDatabase")

    def get_documents_from_user_input(self, filter, token):
        cursor = self.dataset_collection.find({"word": token}, filter)
        samples = [document for document in cursor]
        if len(samples) > 0:
            return samples[0]
        else:
            return None
