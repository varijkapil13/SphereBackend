import os

ROOT_DIR = os.path.dirname(os.path.abspath(os.path.join(__file__, os.pardir, os.pardir)))
DATA_DIR = os.path.join(ROOT_DIR, "backend", "data")
CLASSIFIERS_DIR_SK = os.path.join(ROOT_DIR, "backend", "data", "resources", "classifier", "sklearn")
CLASSIFIERS_DIR_KERAS = os.path.join(ROOT_DIR, "backend", "data", "resources", "classifier",
                                     "keras")
UPLOADED_RESOURCES = os.path.join(ROOT_DIR, "backend", "data", "resources", "uploaded_resources")
FEATURE_EXTRACTORS_DIR = os.path.join(ROOT_DIR, "backend", "data", "resources",
                                      "feature_extractors")
TRAINING_DATA_DIR = os.path.join(ROOT_DIR, "backend", "data", "resources", "training_data")

#######################

GOLD_STANDARDS_DIR = os.path.join(ROOT_DIR, "backend", "data", "resources", "added_gold_standards")
ALL_GOLD_STANDARDS_ZIP_DIR = os.path.join(ROOT_DIR, "backend", "data", "resources")
