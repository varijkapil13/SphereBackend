import csv
import os
import traceback
from io import open

import numpy as np
import spacy

from  libraries.text_classification.backend.mysql.uby.model import GoldStandard, FeatureExtraction
from  libraries.text_classification.backend.utils.constants import GOLD_STANDARDS_DIR, FEATURE_EXTRACTORS_DIR


class MySQLdatasetHandler(object):
    """
        Handles the basic datasets for emotion sentiment gender ...

        Attributes:
           _dataset_name: The name of the dataset that will be saved
           _client: MongoClient
           _dataset_db: Mongo database for text classification
           dataset_collection: Mongo Collection. 
           documents: a list of dictionaries which will be inserted into the collection
    """

    def __init__(self, database):
        self._database = database
        # TODO: REMOVE IF STATEMENT AT FINAL COMMIT!
        x = 0
        for foldername in os.listdir(GOLD_STANDARDS_DIR):
            query = GoldStandard.select(GoldStandard.name).where(
                GoldStandard.name == foldername)
            if not query.exists():
                try:
                    if x < 5:
                        self._insert_gold_standard_from_upload(foldername)
                except FileNotFoundError as error:
                    traceback.print_exc()
            x = x + 1

        for filename in os.listdir(UPLOADED_RESOURCES):
            if os.path.isfile(os.path.join(UPLOADED_RESOURCES, filename)):
                FeatureExtraction.get_or_create(name="uploaded_" + filename.split(".")[0])

    def _insert_gold_standard_from_upload(self, foldername):

        row = {
            "name": foldername,
            "sample": None,
            "labels": None
        }
        rows = []
        with open(os.path.join(GOLD_STANDARDS_DIR, foldername, foldername + ".csv"), "r",
                  newline='',
                  encoding="utf-8") as csvfile:

            reader = csv.reader(csvfile, quotechar='"', delimiter=',')
            try:
                for line in reader:
                    row["sample"] = line[0]

                    labels = ""
                    for label in line[1:]:
                        labels = labels + " " + label
                    row["labels"] = labels.rstrip().lstrip()
                    rows.append(row.copy())
            except (IndexError, UnicodeDecodeError):
                pass

        csvfile.close()

        with self._database.atomic():
            for idx in range(0, len(rows), 100):
                GoldStandard.insert_many(rows[idx:idx + 100]).execute()

    def get_dataset(self, datasetname):

        query = GoldStandard.select(GoldStandard.sample, GoldStandard.labels).where(
            GoldStandard.name == datasetname)
        samples = []
        labels = []
        is_multi_label = False

        for row in query:
            samples.append(row.sample)
            labels.append(row.labels.split(" "))

        for label in labels:
            if len(label) > 1:
                is_multi_label = True

        # tokenize data with spaCy
        nlp = spacy.load("en")
        samples_tokenized = []
        for sample in samples:
            tokens = []
            for token in nlp(sample):
                tokens.append(token.text)
            samples_tokenized.append(tokens)

        return np.array(samples_tokenized), np.array(labels), is_multi_label
