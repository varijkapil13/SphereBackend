import peewee
from peewee import CharField, TextField, BigIntegerField, IntegerField, ForeignKeyField, \
    DoubleField, Model
from playhouse.kv import JSONField

from  libraries.text_classification.backend.mysql.utils.secret import MY_SQL_SECRET_PW

database = peewee.MySQLDatabase("uby", host="localhost", port=3306, user="root",
                                passwd=MY_SQL_SECRET_PW)


class BaseModel(Model):
    class Meta:
        database = database


class Lexicon(BaseModel):
    lexiconid = CharField(primary_key=True)
    lexiconname = CharField()
    languageidentifier = CharField()
    lexicalresourceid = CharField()
    idx = IntegerField()

    class Meta():
        db_table = 'lexicon'


class Polarity(BaseModel):
    polarityid = BigIntegerField(primary_key=True)
    positive = DoubleField()
    negative = DoubleField()

    class Meta():
        db_table = 'polarity'


class Lemma(BaseModel):
    lemmaid = BigIntegerField(primary_key=True)

    class Meta:
        db_table = 'lemma'

    @property
    def written_form(self):
        return self.form_representation.get().writtenform

    @property
    def lexical_entry(self):
        return self.lexical_entry.get()


class LexicalEntry(BaseModel):
    lexicalentryid = TextField(primary_key=True)
    partofspeech = TextField()
    separableparticle = TextField()
    lemma = ForeignKeyField(Lemma, db_column='lemmaid',
                            related_name="lexical_entry", unique=True)
    polarity = ForeignKeyField(Polarity, db_column="polarityid")
    listofcomponentsid = BigIntegerField()
    lexiconid = TextField()

    class Meta():
        db_table = 'lexicalentry'


class FormRepresentationLemma(BaseModel):
    formrepresentationid = BigIntegerField(primary_key=True)
    writtenform = TextField()
    languageidentifier = TextField()
    phoneticform = TextField()
    sound = TextField()
    geographicalvariant = TextField()
    hyphenation = TextField()
    orthographyname = TextField()
    lemma = ForeignKeyField(Lemma, db_column='lemmaid', related_name="form_representation",
                            unique=True)
    idx = IntegerField()

    class Meta():
        db_table = 'formrepresentation_lemma'


class Synset(BaseModel):
    synsetid = TextField(primary_key=True)
    lexiconid = TextField()

    class Meta():
        db_table = 'synset'


class Sense(BaseModel):
    senseid = TextField(primary_key=True)
    synsetid = TextField()
    lexicalentry = ForeignKeyField(LexicalEntry, db_column="lexicalentryid")

    class Meta():
        db_table = 'sense'


class SynsetRelation(BaseModel):
    synsetrelationid = IntegerField(primary_key=True)
    reltype = TextField()
    relname = TextField()
    target = TextField()
    synsetid = TextField()
    idx = IntegerField()


class GoldStandard(BaseModel):
    name = TextField()
    sample = TextField()
    labels = TextField()

    class Meta():
        db_table = "gold_standard"


class Resources(BaseModel):
    name = TextField()
    type = TextField()

    class Meta():
        db_table = "all_resources"


class Workflow(BaseModel):
    name = TextField()
    data = JSONField()

    class Meta():
        db_table = "workflow"


class FeatureExtraction(BaseModel):
    name = TextField()

    class Meta():
        db_table = "feature_extraction"


class GeneralInquirer(BaseModel):
    entry = CharField()
    positiv = CharField()
    negativ = CharField()
    pstv = CharField()
    affil = CharField()
    ngtv = CharField()
    hostile = CharField()
    strong = CharField()
    power = CharField()
    weak = CharField()
    submit = CharField()
    active = CharField()
    passive = CharField()

    class Meta():
        db_table = "general_inquirer"
