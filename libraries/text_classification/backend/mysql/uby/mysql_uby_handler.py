from playhouse.shortcuts import model_to_dict

from  libraries.text_classification.backend.mysql.uby.model import LexicalEntry, \
    FormRepresentationLemma, Sense, Synset, SynsetRelation, GeneralInquirer


class MySQLubyHandler(object):
    def __init__(self, database):
        self._database = database

    def get_lexical_entries(self, lexiconid, lemma):
        lexical_entries = []

        query = LexicalEntry \
            .select(LexicalEntry, FormRepresentationLemma) \
            .join(FormRepresentationLemma,
                  on=(LexicalEntry.lemma == FormRepresentationLemma.lemma).alias("formrep")) \
            .where((FormRepresentationLemma.writtenform == lemma)
                   & (LexicalEntry.lexiconid == lexiconid))

        for lex in query:
            lexical_entries.append(lex)
        return lexical_entries

    def get_senses(self, lexiconid, lemma):
        senses = []
        lexical_entries = self.get_lexical_entries(lexiconid, lemma)

        for lexical_entry in lexical_entries:
            # Sense has an foreign key field called "lexicalentry" on the db_column "lexicalentryid"
            # peewee will automatically create a variable called "lexicalentryid"
            query = Sense.select().where(Sense.lexicalentryid == lexical_entry.lexicalentryid)

            for sense in query:
                senses.append(sense)

        return senses

    def get_polarities(self, lexiconid, lemma):

        polarities = []
        polarity = {
            "positive": None,
            "negative": None
        }
        lexical_entries = self.get_lexical_entries(lexiconid, lemma)

        for lexical_entry in lexical_entries:
            polarity["positive"] = lexical_entry.polarity.positive
            polarity["negative"] = lexical_entry.polarity.negative
            polarities.append(polarity.copy())

        return polarities

    def get_synsets(self, lexiconid, lemma):
        senses = self.get_senses(lexiconid, lemma)
        synsets = []

        for sense in senses:
            query = Synset.select().where(Synset.synsetid == sense.synsetid)

            for synset in query:
                synsets.append(synset)
        return synsets

    def get_synset_relations(self, lexiconid, lemma):
        synsets = self.get_synsets(lexiconid, lemma)
        synsets_relation = []

        for synset in synsets:
            query = SynsetRelation.select().where(SynsetRelation.synsetid == synset.synsetid)

            for relation in query:
                synsets_relation.append(relation)

        return synsets_relation

    # used to get hypernyms, hyponyms from WordNet
    def get_wordnet_relations(self, relname, lemma):

        lexiconid = "WN_Lexicon_0"
        synset_relations = self.get_synset_relations(lexiconid, lemma)
        target_ids = []
        relation = {
            relname: None
        }
        relations = []

        for synset_rel in synset_relations:
            if synset_rel.relname == relname:
                target_ids.append(synset_rel.target)

        for target in target_ids:
            query = Sense.select().where(Sense.synsetid == target)

            for sense in query:
                relation[relname] = sense.lexicalentry.lemma.written_form
                relations.append(relation.copy())

        return relations

    def get_gen_inq_categories(self, word):
        query = GeneralInquirer().select().where(GeneralInquirer.entry == word)
        if query.exists():
            row = query.get()
            categories = model_to_dict(row, exclude=[GeneralInquirer.id, GeneralInquirer.entry])
            categories = {key: value for key, value in categories.items() if value}
            if len(categories) <= 1:
                categories = None
        else:
            categories = None
        return categories
