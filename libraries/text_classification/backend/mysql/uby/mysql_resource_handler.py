from playhouse.shortcuts import model_to_dict

from  libraries.text_classification.backend.mysql.uby.model import GoldStandard, Resources, \
    FeatureExtraction


class MySQLresourceHandler(object):
    """
        Handles the basic datasets for emotion sentiment gender ...

        Attributes:
           _dataset_name: The name of the dataset that will be saved
           _client: MongoClient
           _dataset_db: Mongo database for text classification
           dataset_collection: Mongo Collection. 
           documents: a list of dictionaries which will be inserted into the collection
    """

    def __init__(self, database):
        self._database = database
        self._update_database()

    def _update_database(self):
        query = GoldStandard.select(GoldStandard.name).distinct()
        for resource in query:
            Resources.get_or_create(type="gold_standard", name=resource.name)

        query = FeatureExtraction.select(FeatureExtraction.name).distinct()
        for resource in query:
            Resources.get_or_create(type="feature_extraction",
                                    name=resource.name)

    def get_resources_for_type(self, type=None):
        if type is None:
            query = Resources.select(Resources.name, Resources.type)
        else:
            query = Resources.select(Resources.name, Resources.type).where(Resources.type == type)
        resources = [model_to_dict(resource) for resource in query]

        return resources
