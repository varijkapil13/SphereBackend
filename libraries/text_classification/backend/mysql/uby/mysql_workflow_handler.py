import peewee
import re

from playhouse.shortcuts import model_to_dict

from  libraries.text_classification.backend.mysql.uby.model import Workflow
from  libraries.text_classification.backend.mysql.utils.secret import MY_SQL_SECRET_PW


class MySQLworkflowHandler(object):
    """
        Handles the basic datasets for emotion sentiment gender ...

        Attributes:
           _dataset_name: The name of the dataset that will be saved
           _client: MongoClient
           _dataset_db: Mongo database for text classification
           dataset_collection: Mongo Collection. 
           documents: a list of dictionaries which will be inserted into the collection
    """

    def __init__(self, database):
        self._database = database

    def insert_workflow(self, workflow, scores):

        document = {
            "name": workflow["workflow_name"],
            "status": "trained",
            "workflow": workflow["workflow"],
            "scores": scores
        }

        created_workflow = Workflow(name=workflow["workflow_name"], data=document)
        created_workflow.save()

        return created_workflow.name + "_" + str(created_workflow.id)

    def get_workflows(self, searchstring=None):

        if searchstring is None:
            query = Workflow.select()
        else:
            query = Workflow.select(Workflow.data).where(Workflow.name.contains(searchstring))

        resources = [model_to_dict(workflow) for workflow in query]
        resources_for_api = [resource["data"] for resource in resources]

        return resources_for_api
