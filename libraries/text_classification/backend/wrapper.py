import os

from gensim.models import KeyedVectors

from  libraries.text_classification.backend.mysql.uby.mysql_dataset_handler import \
    MySQLdatasetHandler
from  libraries.text_classification.backend.mysql.uby.mysql_resource_handler import \
    MySQLresourceHandler
from  libraries.text_classification.backend.mysql.uby.mysql_uby_handler import MySQLubyHandler
from  libraries.text_classification.backend.mysql.uby.mysql_workflow_handler import \
    MySQLworkflowHandler
from  libraries.text_classification.classification.utils import get_path_to_file


class ResourceWrapper(object):
    def __init__(self, database):
        self._mysql_dataset_handler = MySQLdatasetHandler(database)
        self._mysql_workflow_handler = MySQLworkflowHandler(database)
        self._mysql_resource_handler = MySQLresourceHandler(database)
        self._mysql_uby_handler = MySQLubyHandler(database)

    def get_training_data(self, datasetname):
        return self._mysql_dataset_handler.get_dataset(datasetname)

    def save_workflow(self, workflow, f1_score, accuracy):
        return self._mysql_workflow_handler.insert_workflow(workflow, {"f1_score": f1_score,
                                                                       "accuracy": accuracy})

    def get_workflows(self, searchstring=None):
        return self._mysql_workflow_handler.get_workflows(searchstring)

    def get_list_of_resources_for_type(self, type=None):
        return self._mysql_resource_handler.get_resources_for_type(type)

    def get_wordnet_relations(self, relname, lemma):
        return self._mysql_uby_handler.get_wordnet_relations(relname, lemma)

    def get_word2vec(self, name):
        if "_tf_idf" in name:
            name = name.replace("_tf_idf", "")

        path, is_binary = get_path_to_file(name)
        pretrained_word2vec_file = os.path.join(path)
        if is_binary:
            model = KeyedVectors.load_word2vec_format(pretrained_word2vec_file, binary=True)
        else:
            model = KeyedVectors.load_word2vec_format(pretrained_word2vec_file, binary=False)
        w2v = {w: vec for w, vec in zip(model.index2word, model.syn0)}

        return w2v

    def get_gen_inq_entry(self, word):
        return self._mysql_uby_handler.get_gen_inq_categories(word)

    def reset_databases(self):
        pass
