Total number of pos tagged words: 106073


+++++++++++++++++++++++++++++Original Description++++++++++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 
====================================================================
        MASC-NEWS Annotating the MASC corpus with BabelNet
        
                 http://lcl.uniroma1.it/MASC-NEWS/
====================================================================

This package contains an automatic annotation of the MASC corpus
using BabelNet 2.0 (http://babelnet.org) meanings.
The format of the annotation follows the one of MASC, i.e., the
GrAF format (http://www.xces.org/ns/GrAF/1.0/).
To see some statistics and information about the annotation and
the used tool please refer to the following paper.

References

Andrea Moro and Roberto Navigli and Francesco M. Tucci and Rebecca
J. Passonneau. Annotating the MASC Corpus with BabelNet. Proc. of
the International Conference on Language Resources and Evaluation
(LREC 2014), Reykjavik, Iceland, May 26-31, 2014

Andrea Moro and Alessandro Raganato and Roberto Navigli. Entity
Linking meets Word Sense Disambiguation: A Unified Approach.
Transactions of the Association for Computational Linguistics
(TACL), 2:231-244, 2014