Total number of sentences  :3178
 
 
 
	Aim 	AIMX 	The specific research goal of the paper 
	Own 	OWNX 	The author’s own work, e.g. methods, results, conclusions 
	Contrast 	CONT 	Contrast, comparison or critique of past work 
	Basis 	BASE 	Past work that provides the basis for the work in the article. 
	Misc 	MISC 	Any other sentences 