import os

import en_core_web_sm
import numpy as np

from  libraries.text_classification.backend.utils.constants import FEATURE_EXTRACTORS_DIR

word2vec_conditions = ["word2vec_affectivespace", "word2vec_google", "word2vec_amazon",
                       "word2vec_isacore"]
word2vec_conditions_tf_idf = ["word2vec_affectivespace_tf_fdf", "word2vec_google_tf_fdf",
                              "word2vec_amazon_tf_fdf", "word2vec_isacore_tf_fdf"]
wordnet_conditions = ["hypernym", "hyponym"]


def getSentiWordNetTag(pos_tag):
    newtag = None

    if pos_tag.startswith("NN"):
        newtag = 'n'
    elif pos_tag.startswith("JJ"):
        newtag = 'a'
    elif pos_tag.startswith("V"):
        newtag = "v"
    elif pos_tag.startswith("R"):
        newtag = "r"
    return newtag


def get_scores_for_mongo(f1_score, accuracy):
    return {
        "f1_score": f1_score,
        "accuracy": accuracy
    }


def get_all_nouns_per_sample(untokenized_X):
    nouns_per_sample = []
    nlp = en_core_web_sm.load()
    NN = nlp.vocab.strings['NN']
    NNS = nlp.vocab.strings['NNS']
    NNP = nlp.vocab.strings['NNP']
    NNPS = nlp.vocab.strings['NNPS']

    for sample in untokenized_X:
        nouns = [token.lemma_ if token.tag == NN or
                                 token.tag == NNS or
                                 token.tag == NNP or
                                 token.tag == NNPS else ''
                 for token in nlp(str(sample))]
        nouns = list(filter(None, nouns))
        nouns_per_sample.append(nouns)

    return nouns_per_sample


def get_all_pos_per_sample(untokenized_X):
    pos_per_sample = []
    nlp = en_core_web_sm.load()

    for sample in untokenized_X:
        pos_tags = [token.pos_ for token in nlp(str(sample))]
        nouns = list(filter(None, pos_tags))
        pos_per_sample.append(nouns)

    return pos_per_sample


def get_all_gen_inq_values_per_sample(X, resource_wrapper):
    gen_inq_values_per_sample = []

    for sample in X:
        values = [resource_wrapper.get_gen_inq_entry(token) for token in sample]
        values = list(filter(None, values))
        gen_inq_values_per_sample.append(values)

    return gen_inq_values_per_sample


def merge_dicts_per_sample(values_per_sample):
    features = []
    for values in values_per_sample:
        super_dict = {}
        for dict in values:
            super_dict = {**super_dict, **dict}
        features.append(super_dict.copy())
    return features


def untokenize_training_data(X):
    X_untokenized = []

    for sample in X:
        sample_untokenized = ""
        for token in sample:
            sample_untokenized = sample_untokenized + " " + token

        sample_untokenized = sample_untokenized.rstrip().lstrip()
        X_untokenized.append(sample_untokenized)

    return np.array(X_untokenized)


def get_path_to_file(name):
    path = os.path.join(FEATURE_EXTRACTORS_DIR, name + ".bin")
    is_binary = True
    if os.path.isfile(path):
        return path, is_binary
    else:
        path = os.path.join(FEATURE_EXTRACTORS_DIR, name + ".txt")
        is_binary = False
        return path, is_binary


def create_dir(dirpath):
    if not os.path.exists(dirpath):
        os.makedirs(dirpath)
