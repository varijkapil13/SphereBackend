import os
import dill
from  libraries.text_classification.backend.utils.constants import CLASSIFIERS_DIR_SK, CLASSIFIERS_DIR_KERAS
from keras.models import load_model

class Predictions:
    def __init__(self, workflow_id, predition_text, line_separator, split):
        self._clf = dill.load(file=open(os.path.join(CLASSIFIERS_DIR_SK, workflow_id + ".pkl"), "rb"))
        if split == True:
            self._predition_text = (line_separator + "\n").join(predition_text.split('\n'))
        else:
            self._predition_text = predition_text

        self._line_separator = line_separator

    def return_predictions(self):
        splitted_text = self._predition_text.split(self._line_separator)
        predictions = self._clf.make_prediction(self._predition_text.split(self._line_separator))
        prediction_result = []
        for predition, text in zip(predictions, splitted_text):
            prediction_result.append({
                'text'      : text,
                'prediction': predition.copy()
            })
        return prediction_result


class Prediction:
    def __init__(self, workflow_id, predition_text, line_separator, split):
        if split == True:
            self._predition_text = (line_separator + "\n").join(predition_text.split('\n'))
        else:
            self._predition_text = predition_text

        self._line_separator = line_separator
        self._workflow_id = workflow_id

    def return_predictions(self):
        sample = self._predition_text
        splitted_text = self._predition_text.split(self._line_separator)
        keras_prediction = True
        try:
            path = os.path.join(CLASSIFIERS_DIR_KERAS, self._workflow_id + ".h5")
            model = load_model(path)
        except FileNotFoundError:
            keras_prediction = False
            pass

        path = os.path.join(CLASSIFIERS_DIR_SK, self._workflow_id + ".pkl")
        pipeline = dill.load(file=open(path, "rb"))
        if not keras_prediction:
            predictions = pipeline.make_prediction([sample.split(self._line_separator)])
        elif keras_prediction:
            pipeline.reconstruct_keras_pipeline(model)
            predictions = pipeline.make_prediction([sample.split(self._line_separator)])

        prediction_result = []
        for predition, text in zip(predictions, splitted_text):
            prediction_result.append({
                'text'      : text,
                'prediction': predition.copy()
            })
        return prediction_result