import os

from keras.layers import Dense
from keras.layers import Input
from keras.models import Model, Sequential

backend_path = os.path.join(os.pardir, "backend")

embeddingPathGoogle = os.path.join("embeddings", "embeddings.npz")
embeddingPathFast = os.path.join("embeddings", "fast_model", "embeddings.npz")


class GenericDeepLearningClassifierCreator(object):
    """
    Generic neural network

    Attributes:
        model: An object representing the keras deep learning model
        _number_of_classes: Integer which defines to number of output classes of the classifier. Used in the last layer of keras model
    """

    def __init__(self, number_of_classes, network_type):
        self.model = None
        self._input_dimension = None
        self._number_of_classes = number_of_classes
        self._network_type = network_type

    def _create_CNN(self):
        self.model = Sequential()
        self.model.add(Dense(32, input_dim=self._input_dimension))
        self.model.add(Dense(64, activation="relu"))
        self.model.add(Dense(64, activation="relu"))
        self.model.add(Dense(self._number_of_classes, activation="softmax"))
        self.model.compile(loss='sparse_categorical_crossentropy', optimizer='rmsprop',
                           metrics=['accuracy'])


    def _create_CNN_functional(self):
        inputs = Input(shape=(self._input_dimension,), sparse=True)
        x = Dense(64, activation='relu')(inputs)
        x = Dense(64, activation='relu')(x)
        predictions = Dense(self._number_of_classes, activation='softmax')(x)
        self.model = Model(inputs=inputs, outputs=predictions)
        self.model.compile(loss='sparse_categorical_crossentropy', optimizer='rmsprop',
                           metrics=['accuracy'])

    def _create_MLP_functional(self):
        pass

    def _create_LSTM_functional(self):
        pass

    def get_neural_network(self):
        return self.model

    def set_input_dimension(self, dimension):
        self._input_dimension = dimension

    def create_neural_network(self):
        if self._network_type == "cnn":
            self._create_CNN()
        if self._network_type == "mlp":
            self._create_MLP_functional()
        if self._network_type == "lstm":
            self._create_LSTM_functional()