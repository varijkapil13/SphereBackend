import os

import dill
import numpy as np
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.metrics import accuracy_score, f1_score
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.multiclass import OneVsRestClassifier
from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.svm import SVC

from  libraries.text_classification.backend.utils.constants import CLASSIFIERS_DIR_SK, \
    CLASSIFIERS_DIR_KERAS
from  libraries.text_classification.classification.deep_learning_creator import \
    GenericDeepLearningClassifierCreator
from  libraries.text_classification.classification.link_checker import LinkChecker, LinkCheckerError
from  libraries.text_classification.classification.transformers import TfidfEmbeddingVectorizer, \
    MeanEmbeddingVectorizer, SentimentTransformer, WorkflowTransformer, WordNetTransformer, \
    KerasTransformer, PosTagTransformer, GenInqTransformer, UserUploadTransformer
from  libraries.text_classification.classification.utils import word2vec_conditions, \
    word2vec_conditions_tf_idf, wordnet_conditions


class GenericMLclassifierCreator(object):
    """
    TODO:
    Attributes:
        TODO:
    """

    def __init__(self, workflow_data, resource_wrapper):

        fe_methods = [node for node in workflow_data["nodes"] if node["type"] == "fe"]
        classification_methods = [node for node in workflow_data["nodes"] if node["type"] == "cf"]
        gold_standard_methods = [node for node in workflow_data["nodes"] if node["type"] == "gs"]
        custom_workflows = [node for node in workflow_data["nodes"] if node["type"] == "cw"]

        link_checker = LinkChecker(nodes=workflow_data["nodes"], links=workflow_data["links"])
        try:
            link_checker.check_gs_to_fe()
            link_checker.check_fe_to_cf()
        except LinkCheckerError:
            raise

        self._workflow_data = workflow_data
        self._resource_wrapper = resource_wrapper
        self._gold_standard_methods = gold_standard_methods
        self._feature_extraction_methods = fe_methods
        self._custom_workflows = custom_workflows
        self._classification_method = classification_methods
        self._features_from_workflow = None
        self._classifier = None
        self._keras_creator = None
        self._accuracy = None
        self._f1_score = None
        self._classifier_id = None
        self._multi_label_binarizer = None
        self._X = None
        self._y = None
        self._is_multi_label = None
        self._features = None
        self._pipeline = None

        self._X, self._y, self._is_multi_label = \
            self._resource_wrapper.get_training_data(self._get_gold_standard())

        if self._is_multi_label:
            self._multi_label_binarizer = MultiLabelBinarizer()
            self._y = self._multi_label_binarizer.fit_transform(self._y)
        print("Data loaded. Total number of samples %s" % len(self._y))

    def start_building_classifier(self):
        # classifier method
        self._classifier, self._features_from_workflow, self._keras_creator = self._init_classifier()

        # returns a sklearn union with corresponding feature extraction methods
        self._features = self._init_features()

        # based on sklearn.pipeline.pipeline
        self._pipeline = self._pipe_classifier()

        # training
        self._train_classifier()

        # save model
        self._save_classifier()

    def _get_gold_standard(self):
        return self._gold_standard_methods[0]["name"]

    def _init_classifier(self):
        classifier = None
        features = None
        keras_creator = None

        for cf_method in self._classification_method:
            if cf_method["name"] == "svm":
                if self._is_multi_label:
                    classifier = OneVsRestClassifier(SVC(kernel="linear"))
                else:
                    classifier = SVC(kernel="linear")

            if cf_method["name"] == "extra_trees":
                classifier = ExtraTreesClassifier(n_estimators=200)

            if cf_method["name"] == "keras":
                keras_creator = GenericDeepLearningClassifierCreator(
                    number_of_classes=np.unique(self._y).shape[0],
                    network_type="cnn")
                classifier = KerasClassifier(build_fn=keras_creator.get_neural_network,
                                             epochs=1,
                                             batch_size=5)

            if cf_method["name"] == "workflow":
                workflow = dill.load(
                    file=open(os.path.join(CLASSIFIERS_DIR_SK, cf_method["id"] + ".pkl"),
                              "rb"))
                features = workflow.get_features()
                classifier = workflow.get_classifier()

        return classifier, features, keras_creator

    def _init_features(self):
        transformer_list = []

        for fe_method in self._feature_extraction_methods:
            if any(fe_method["name"] in word2vec_method for word2vec_method in word2vec_conditions):
                w2v = self._resource_wrapper.get_word2vec(name=fe_method["name"])
                transformer_list.append(("word2vec", MeanEmbeddingVectorizer(w2v)))
            if any(fe_method["name"] in word2vec_method for word2vec_method in
                   word2vec_conditions_tf_idf):
                w2v = self._resource_wrapper.get_word2vec(name=fe_method["name"])
                transformer_list.append(("word2vec_tf_idf", TfidfEmbeddingVectorizer(w2v)))
            if fe_method["name"] == "tf_idf":
                # Data is already tokenized with spaCy (better than nltk/sklearn tokenizer)
                # Therefore we have to change the analyzer of the vectorizer
                transformer_list.append(("tf_idf", TfidfVectorizer(analyzer=lambda x: x)))
            if fe_method["name"] == "sentiment_sentiwordnet":
                sent_transformer = ("sentiment", Pipeline([
                    ("sentstats", SentimentTransformer()),
                    ("vect", DictVectorizer())
                ]))
                transformer_list.append(sent_transformer)
            if any(fe_method["name"] in relation for relation in wordnet_conditions):
                wordnet_transformer = ("wordnet", Pipeline([
                    ("wordnetstats", WordNetTransformer(self._resource_wrapper, fe_method["name"])),
                    ("vect", DictVectorizer())
                ]))
                transformer_list.append(wordnet_transformer)
            if fe_method["name"] == "pos_tag":
                pos_tag_transformer = ("pos_tag", Pipeline([
                    ("pos_tag_stats", PosTagTransformer()),
                    ("vect", DictVectorizer())
                ]))
                transformer_list.append(pos_tag_transformer)
            if fe_method["name"] == "gen_inq":
                gen_inq_transformer = ("gen_inq", Pipeline([
                    ("gen_inq_stats", GenInqTransformer(self._resource_wrapper)),
                    ("vect", DictVectorizer())
                ]))
                transformer_list.append(gen_inq_transformer)
            if "uploaded" in fe_method["name"]:
                lexiconname = fe_method["name"][9:]
                uploaded_json_transformer = ("uploaded", Pipeline([
                    ("uploaded_stats",
                     UserUploadTransformer(lexiconname)),
                    ("vect", DictVectorizer())
                ]))
                transformer_list.append(uploaded_json_transformer)

        # already created workflow used as feature extractor node
        if self._custom_workflows:
            for index, wf in enumerate(self._custom_workflows):
                workflow_transformer = ("custom_workfow_pipeline_" + str(index), Pipeline([
                    ("workflow_features", WorkflowTransformer(wf["id"])),
                    ("vect", DictVectorizer())
                ]))
                transformer_list.append(workflow_transformer)

        # already created workflow used as classifer node
        if self._features_from_workflow is not None:
            transformer_list.append(self._features_from_workflow)

        # if no features added, make most basic classifier possible
        if not transformer_list:
            transformer_list.append(("vect", CountVectorizer(analyzer=lambda x: x)))

        union = ('union', FeatureUnion(
                transformer_list=transformer_list
        ))

        return union

    def _pipe_classifier(self):

        return Pipeline([
            self._features,
            ('keras', KerasTransformer(self._keras_creator)),
            ("classifier", self._classifier)
        ])

    def _train_classifier(self):
        train_size = np.math.floor(len(self._y) * 3 / 4)
        test_size = 1 - (train_size / float(len(self._y)))

        accuracy_scores = []
        f1_scores = []

        sss = StratifiedShuffleSplit(n_splits=5, test_size=test_size, random_state=0)
        for train_index, test_index in sss.split(self._X, self._y):
            X_train, X_test = self._X[train_index], self._X[test_index]
            y_train, y_test = self._y[train_index], self._y[test_index]

            y_pred = self._pipeline.fit(X_train, y_train).predict(X_test)
            accuracy_scores.append(accuracy_score(y_true=y_test, y_pred=y_pred))
            f1_scores.append(f1_score(y_true=y_test, y_pred=y_pred, average="macro"))

        self._set_accuracy(np.mean(accuracy_scores))
        self._set_f1_score(np.mean(f1_scores))
        print("Finished Training")

    def _set_accuracy(self, accuracy):
        self._accuracy = accuracy

    def _set_f1_score(self, score):
        self._f1_score = score

    def _save_keras_(self):
        path = os.path.join(CLASSIFIERS_DIR_KERAS, self._classifier_id + ".h5")
        self._classifier.model.save(path)
        # This is really ugly. We need to set everything to None what has a connection with keras
        # so dill can dump the rest of the pipeline
        # when the user wants to access his saved classifier the pipeline will be constructed
        # with the .pkl file and then the keras model will be included
        # see method: reconstruct_keras_pipeline
        self._classifier = None
        self._keras_creator = None
        self._pipeline.steps[1] = None
        self._pipeline.steps[2] = None

    def _save_sklearn(self):
        path = os.path.join(CLASSIFIERS_DIR_SK, self._classifier_id + ".pkl")
        dill.dump(self, open(path, "wb"))
        print("saved workflow + classifier")

    def _save_classifier(self):
        create_dir(CLASSIFIERS_DIR_SK)
        create_dir(CLASSIFIERS_DIR_KERAS)

        self._classifier_id = self._resource_wrapper.save_workflow(
            self._workflow_data, self._f1_score, self._accuracy)

        # Close connection to DB because now the file will be saved
        # otherwise dill can't dump file
        self._resource_wrapper = None

        # dill or pickle cannot save keras model
        if type(self._classifier) == KerasClassifier:
            self._save_keras_()
        self._save_sklearn()

    def get_classifier(self):
        return self._classifier

    def get_features(self):
        return self._features

    def get_classifier_id(self):
        return self._classifier_id

    def make_prediction(self, sample):
        prediction = self._pipeline.predict(sample)
        if self._is_multi_label:
            return self._multi_label_binarizer.inverse_transform(prediction)
        else:
            return prediction

    def reconstruct_keras_pipeline(self, model):
        # THIS IS UGLY AS FUCK
        keras_creator = GenericDeepLearningClassifierCreator(
            number_of_classes=np.unique(self._y).shape[0],
            network_type="cnn")
        self._classifier = KerasClassifier(build_fn=keras_creator.get_neural_network,
                                           epochs=1,
                                           batch_size=5)

        self._classifier.classes_ = np.unique(self._y)
        self._classifier.n_classes_ = np.unique(self._y)[0]
        self._classifier.model = model
        self._pipeline.steps[1] = ('keras', KerasTransformer(keras_creator))
        self._pipeline.steps[2] = ('classifier', self._classifier)


# multi_naive_bayes and word2vec will not work and is not supposed to work,
#  because features have to be naive!
if __name__ == "__main__":
    pass
