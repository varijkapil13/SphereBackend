class LinkChecker(object):
    def __init__(self, nodes, links):
        self._nodes = nodes
        self._links = links

    def check_gs_to_fe(self):
        sources = [node["id"] for node in self._nodes if node["type"] == "gs"]
        targets = [node["id"] for node in self._nodes if node["type"] == "fe"]

        for link in self._links:
            if link["source"] in sources:
                if link["target"] not in targets:
                    raise LinkCheckerError(message="Wrong data")

    def check_fe_to_cf(self):
        sources = [node["id"] for node in self._nodes if node["type"] == "fe"]
        targets = [node["id"] for node in self._nodes if node["type"] == "cf"]

        for link in self._links:
            if link["source"] in sources:
                if link["target"] not in targets:
                    raise LinkCheckerError(message="Wrong data")


class LinkCheckerError(Exception):
    """Exception raised for wrong workflow data

     Attributes:
         message -- explanation of the error
     """

    def __init__(self, message):
        self.message = message