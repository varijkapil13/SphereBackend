import os
from collections import defaultdict

import dill
import nltk
import numpy as np
from nltk.corpus import sentiwordnet as swn
from sklearn.feature_extraction.text import TfidfVectorizer

from  libraries.text_classification.backend.mongo.mongo_upload_handler import MongoUploadHandler
from  libraries.text_classification.backend.utils.constants import CLASSIFIERS_DIR_SK
from  libraries.text_classification.classification.utils import getSentiWordNetTag, \
    get_all_nouns_per_sample, untokenize_training_data, get_all_pos_per_sample, \
    get_all_gen_inq_values_per_sample, merge_dicts_per_sample


class TfidfEmbeddingVectorizer(object):
    def __init__(self, word2vec):
        self.word2vec = word2vec
        self.word2weight = None
        self.dim = len(next(iter(word2vec.values())))

    def fit(self, X, y):
        # Data is already tokenized with spaCy (better than nltk/sklearn)
        # Therefore we have to change the analyzer of the vectorizer
        tfidf = TfidfVectorizer(analyzer=lambda x: x)

        tfidf.fit(X)
        # if a word was never seen - it must be at least as infrequent
        # as any of the known words - so the default idf is the max of
        # known idf's
        max_idf = max(tfidf.idf_)
        self.word2weight = defaultdict(
            lambda: max_idf,
            [(w, tfidf.idf_[i]) for w, i in tfidf.vocabulary_.items()])

        return self

    def transform(self, X):
        return np.array([
            np.mean([self.word2vec[w] * self.word2weight[w]
                     for w in words if w in self.word2vec] or
                    [np.zeros(self.dim)], axis=0)
            for words in X
        ])


class MeanEmbeddingVectorizer(object):
    def __init__(self, word2vec):
        self.word2vec = word2vec
        self.dim = len(next(iter(word2vec.values())))

    def fit(self, X, y):
        return self

    def transform(self, X):
        return np.array([
            np.mean([self.word2vec[w] for w in words if w in self.word2vec]
                    or [np.zeros(self.dim)], axis=0)
            for words in X
        ])


class SentimentTransformer(object):
    """ 
    Extracts the sentiment and the number of words for each sample for DictVectorizer 
    """

    def fit(self, x, y=None):
        return self

    def transform(self, X):
        feature_vectors = []

        for sample in X:
            tagged_tokens = nltk.pos_tag(sample)
            number_of_words = 0
            positive_score = 0
            negative_score = 0
            features = {
                'num_words': None,
                'sentiment': None
            }

            for index, token in enumerate(sample):
                senti_wordnet_tag = getSentiWordNetTag(tagged_tokens[index][1])
                if senti_wordnet_tag is not None:
                    try:
                        word_sentiment = swn.senti_synset(
                            token + "." + senti_wordnet_tag + "." + "01")
                        positive_score = positive_score + word_sentiment.pos_score()
                        negative_score = negative_score + word_sentiment.neg_score()
                        number_of_words = number_of_words + 1
                    except Exception as e:
                        print(e)

            if number_of_words is not None:
                sentiment_of_words = positive_score - negative_score
                features["sentiment"] = sentiment_of_words
                features["num_words"] = number_of_words
                feature_vectors.append(features.copy())
        return feature_vectors


class WorkflowTransformer(object):
    def __init__(self, id):
        self._clf = dill.load(file=open(os.path.join(CLASSIFIERS_DIR_SK, id + ".pkl"), "rb"))

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        feature_vectors = []
        for sample in X:
            features = {
                "prediction": None
            }
            predictions = self._clf.make_prediction([sample])

            features["prediction"] = predictions[0]
            feature_vectors.append(features.copy())
        return feature_vectors


class WordNetTransformer(object):
    def __init__(self, resource_wrapper, wordnet_relation=None):
        self._resource_wrapper = resource_wrapper
        self._wordnet_relation = wordnet_relation
        pass

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        untokenized_X = untokenize_training_data(X)
        self._wordnet_relation = self._wordnet_relation
        feature_vectors = []

        nouns_per_sample = get_all_nouns_per_sample(untokenized_X)
        relations_per_sample = self._get_relations_per_sample(nouns_per_sample)

        for relations in relations_per_sample:
            feature = {self._wordnet_relation + "_" + str(idx): relation for idx, relation in
                       enumerate(relations)}
            feature_vectors.append(feature)

        return feature_vectors

    def _get_relations_per_sample(self, part_of_speech_per_sample):
        relations_per_sample = []
        for idx, nouns in enumerate(part_of_speech_per_sample):
            relations_for_nouns = []
            for noun in nouns:
                relations_for_noun = self._resource_wrapper.get_wordnet_relations(
                    self._wordnet_relation, noun)
                relations_for_nouns.extend(
                    [relation_dict[self._wordnet_relation] for relation_dict in relations_for_noun])
            relations_per_sample.append(relations_for_nouns)
        return relations_per_sample


class KerasTransformer(object):
    def __init__(self, keras_creator):
        self._keras_creator = None
        if keras_creator is not None:
            self._keras_creator = keras_creator
        pass

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        if self._keras_creator is not None:
            self._keras_creator.set_input_dimension(X.shape[1])
            self._keras_creator.create_neural_network()
            X = X.todense()
        return X


class PosTagTransformer(object):
    def __init__(self):
        pass

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        untokenized_X = untokenize_training_data(X)
        feature_vectors = []

        pos_per_sample = get_all_pos_per_sample(untokenized_X)
        for pos_tags in pos_per_sample:
            feature = {"pos" + "_" + str(idx): pos_tag for idx, pos_tag in
                       enumerate(pos_tags)}
            feature_vectors.append(feature.copy())

        return feature_vectors


class GenInqTransformer(object):
    def __init__(self, resource_wrapper):
        self._resource_wrapper = resource_wrapper

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        values_per_sample = get_all_gen_inq_values_per_sample(X, self._resource_wrapper)
        feature_vectors = merge_dicts_per_sample(values_per_sample)

        return feature_vectors


class UserUploadTransformer(object):
    def __init__(self, lexiconname):
        self._lexiconname = lexiconname
        pass

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        feature_vectors = []
        upload_handler = MongoUploadHandler(self._lexiconname)

        keydb = DatabaseFunctions().find_item(LexiconsConstants.database,
                                              LexiconsConstants.collection,
                                              LexiconsConstants.lc_name, self._lexiconname)
        filter_values = {key["key"]: 1 for idx, key in enumerate(keydb["user_keys"])}
        filter_values["_id"] = 0

        list_of_dicts_per_sample = []

        for sample in X:
            list_of_dicts_one_sample = []
            for token in sample:
                list_of_dicts_for_token = upload_handler.get_documents_from_user_input(
                    filter_values, token)
                list_of_dicts_one_sample.append(list_of_dicts_for_token)

            list_of_dicts_one_sample = list(filter(None, list_of_dicts_one_sample))
            list_of_dicts_per_sample.append(list_of_dicts_one_sample)

        user_input = [key["key"] for key in keydb["user_keys"]]
        for list_of_dict_one_sample in list_of_dicts_per_sample:
            feature = {}
            for outer_idx, dict in enumerate(list_of_dict_one_sample):
                for inner_idx, keys in enumerate(user_input):
                    keys = keys.split(".")
                    temp = dict
                    for key in keys:
                        temp = temp[key]
                        if isinstance(temp, str):
                            test = {user_input[inner_idx] + "_" + str(outer_idx): temp}
                            feature = {**feature, **test}
                        pass
                    pass
            feature_vectors.append(feature.copy())

        return feature_vectors
