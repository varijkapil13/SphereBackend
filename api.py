#!flask/bin/python3
import hashlib
import os
import sys
import tarfile
import traceback
import json
import dill
import peewee
from celery import Celery
from flask import Flask, jsonify, request, abort, make_response, send_file
from flask_cors import CORS
from flask_restful import Api, Resource
from keras.models import load_model
from libraries.image_classification.FeatureExtraction import FeatureExtraction
from libraries.image_classification.ImageDownloader import ImageDownloader
from libraries.image_classification.Training import Training
from libraries.image_classification.ValidationClasses import ModelValidation
from libraries.image_classification.constants import GoldStandardConstants as gsc, \
	ModelMongoConstants as modelConstants, ImageDatasetConstants as imageDatasetConstants, LexiconsConstants
from libraries.image_classification.utils import Constants, PrintHelper, \
	DatabaseJSONStructure, DatabaseFunctions, Response, prediction_images_dir, main_image_data_dir
from libraries.text_classification.backend.mysql.utils.secret import MY_SQL_SECRET_PW
from libraries.text_classification.backend.utils.constants import CLASSIFIERS_DIR_SK, \
	GOLD_STANDARDS_DIR as gold_standards_dir, CLASSIFIERS_DIR_KERAS, UPLOADED_RESOURCES
from libraries.text_classification.backend.wrapper import ResourceWrapper
from libraries.text_classification.classification.annotation_handler import Predictions, Prediction
from libraries.text_classification.classification.machine_learning_creator import \
	GenericMLclassifierCreator
from libraries.text_classification.resource_creation.document_parser import DocumentParser

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

app = Flask(__name__)
env = os.environ

CELERY_BROKER_URL = env.get('CELERY_BROKER_URL', 'redis://localhost:6379/0'),
CELERY_RESULT_BACKEND = env.get('CELERY_RESULT_BACKEND', 'redis://localhost:6379/0')
# CELERY_BROKER_URL = env.get('CELERY_BROKER_URL', 'redis://redis:6379/0'),
# CELERY_RESULT_BACKEND = env.get('CELERY_RESULT_BACKEND', 'redis://redis:6379/0')

app.config['CORS_HEADERS'] = 'Content-Type'
app.config['CELERY_BROKER_URL'] = CELERY_BROKER_URL
app.config['CELERY_RESULT_BACKEND'] = CELERY_RESULT_BACKEND

celery = Celery(app.name,
                broker=CELERY_BROKER_URL,
                backend=CELERY_RESULT_BACKEND)
celery.conf.update(app.config)

cors = CORS(app)
api = Api(app)

"""
Image Classification
"""


@app.route('/sphere/api/v1/recieveImages', methods=['POST'])
def download_dataset_images():
	if not request.json or 'images' not in request.json:
		abort(400)
	try:
		image_list = request.json['images']
		background_image_downloader.delay(image_list)
		return Response.create_success_response("Images Downloaded",
		                                        "Images are being downloaded. Once downloaded, "
		                                        "they will be available for training")

	except Exception as e:
		PrintHelper().failure_print(e)
		return Response.create_failure_response('Could not download images with Error:  ' + str(e))


@app.route('/sphere/api/v1/startTraining', methods=['POST'])
def image_transfer_and_training():
	"""

	:return:
	"""

	if not request.json or 'datasets' not in request.json or 'model' not in request.json:
		abort(400)

	try:

		dataset_items = request.json['datasets']
		PrintHelper().info_print(dataset_items)
		dataset_string = ','.join(dataset_items)

		selected_model_name = request.json['model']
		save_name_for_model = request.json['name']

		encoded_keys = (dataset_string + selected_model_name).encode('utf-8')
		classes_hash_key = hashlib.sha256(encoded_keys)
		hash_digest = classes_hash_key.hexdigest()
		hash_digest = save_name_for_model + "###" + hash_digest
		models_result = DatabaseFunctions().find_item(
				modelConstants.database,
				modelConstants.collection,
				modelConstants.mm_model_hash, hash_digest)

		if models_result is None:
			PrintHelper().info_print('Model was not found, continue with the processing')

			DatabaseFunctions().insert_item(
					database_name=modelConstants.database,
					collection_name=modelConstants.collection,
					items_to_insert=DatabaseJSONStructure.insert_new_model(dataset_string, hash_digest,
					                                                       selected_model_name,
					                                                       model_save_name=save_name_for_model))

			status, number_of_samples, number_of_classes = ImageDownloader().transfer_datasets_and_start_training(
					dataset_items, hash_digest)
			if status:
				start_background_training.delay(selected_model_name, hash_digest, number_of_classes,
				                                number_of_samples)

				return Response().create_success_response('',
				                                          'Training Started on selected Model')
			else:
				return Response().create_failure_response(
						'An already trained or currently in training model is already '
						'present in the database. Please check "View and Validate" tab to '
						'view and test this model')
		else:
			PrintHelper().warning_print('Model already present')
			return Response().create_failure_response(
					'An already trained or currently in training model is already '
					'present in the database. Please check "View and Validate" tab to'
					' view and test this model')

	except Exception as e:
		PrintHelper().failure_print(e)
		return Response.create_failure_response(
				'Exception Occured: image_transfer_and_training' + str(e))


@app.route('/sphere/api/v1/getAvialableImageDatasets', methods=['GET'])
def get_image_dataset_list():
	"""

	:return:
	"""
	try:
		image_dataset = DatabaseFunctions().find_all_items(imageDatasetConstants.database,
		                                                   imageDatasetConstants.collection)
		return Response.create_success_response(image_dataset, 'Dataset fetched successfully')
	except Exception as e:
		PrintHelper().failure_print(e)
		return Response.create_failure_response('Database Error')


@app.route('/sphere/api/v1/getModels', methods=['GET'])
def get_saved_models():
	"""

	:return:
	"""
	if request.method != 'GET':
		abort(403)

	try:
		saved_models = DatabaseFunctions().find_all_items(database_name=modelConstants.database,
		                                                  collection_name=modelConstants.collection)

		if saved_models is None:
			PrintHelper().failure_print('No models found in the database')

			return Response.create_failure_response('No saved models found')

		else:
			return Response.create_success_response(saved_models, 'Fetched models successfully')
	except Exception as e:

		PrintHelper().failure_print('sphere_WebAPI', e)
		return Response.create_failure_response('Database error')


@app.route('/sphere/api/v1/startValidation', methods=['POST'])
def start_validation():
	"""

	:return:
	"""
	if request.method != 'POST':
		abort(403)

	if not request.json or 'model' not in request.json or 'hash' not in request.json:
		abort(400)
	try:

		model_name = request.json['model']
		hash_key = request.json['hash']
		PrintHelper().info_print('Model Name', model_name)
		start_background_validation.delay(model_name, hash_key)
		return Response.create_success_response('Validation Started', 'Validation Started')

	except Exception as e:
		PrintHelper().failure_print(e)
		return Response.create_failure_response('Could not start validation process')


''' Prediction API'''


@app.route('/sphere/api/v1/prediction', methods=['POST'])
def start_predictions():
	try:
		form = request.form

		model_name = ""

		# Needed only with custom model training
		base_model = None
		model_hash = None
		######

		for key, value in form.items():
			if key == 'model_hash':
				model_hash = value
			if key == 'model_name':
				model_name = value
			if key == 'model_base_name':
				base_model = value

		image_count = 0
		image_paths = ''
		save_files = request.files.getlist("file")
		for upload in request.files.getlist("file"):
			filename = upload.filename.rsplit("/")[0]
			PrintHelper.info_print("Uploaded: ", filename)
			destination_main = os.path.dirname(
					os.path.realpath(__file__)) + prediction_images_dir + '/'
			destination = destination_main + model_name + '/'
			Constants.directory_validation(destination)
			upload.save(destination + filename)
			image_paths = destination_main
			image_count = + 1

		feature_extraction = FeatureExtraction(model_name, image_paths, image_count)
		predictions = feature_extraction.get_predictions()

		# converting the complex tuple structre into javascript friendly json object
		count = 0
		prediction_array = []
		for item in predictions:
			result_outer = []
			for items in item:
				result_inner = {
					"class"      : items[0],
					"description": items[1],
					"probability": str(items[2])
				}
				result_outer.append(result_inner)

			image_file = save_files[count]
			count = + 1

			image_file_name = image_file.filename.rsplit("/")[0]

			final_prediction_result = {
				"filename"   : image_file_name,
				"predictions": result_outer
			}
			prediction_array.append(final_prediction_result)

		return Response.create_success_response(prediction_array, "success")

	except Exception as e:
		PrintHelper.failure_print("An error occurered:  " + str(e))
		return Response.create_failure_response("An error occurred: " + str(e))


@app.route('/sphere/api/v1/imageDatasetUpload', methods=['POST'])
def image_dataset_upload():
	try:
		form = request.form

		class_name = ""

		for key, value in form.items():
			if key == 'class_name':
				class_name = value

		image_file_list = request.files.getlist("file")

		ImageDownloader().recieve_image_dataset_from_user(image_file_list, class_name)

		return Response.create_success_response("Images Downloaded successfully", "success")

	except Exception as e:
		PrintHelper.failure_print("An error occurered:  " + str(e))
		return Response.create_failure_response("An error occurred: " + str(e))


"""
Text Classification
"""

''' Text Annotation ===> Prediction Api'''


@app.route('/sphere/api/v1/workflow/<id_workflow>/predict', methods=['POST'])
def perform_annotations(id_workflow):
	"""

	:param id_workflow:
	:return:
	"""
	if request.method != 'POST':
		abort(403)

	if not request.json or 'text' not in request.json or 'separator' not in request.json or 'split' not in request.json:
		abort(400)

	try:
		annotate = Prediction(id_workflow, request.json['text'], request.json['separator'],
		                      request.json['split'])
		annotation_results = annotate.return_predictions()

		return Response.create_success_response(annotation_results, 'Annotation Completed')

	except Exception as e:
		PrintHelper().failure_print(e)
		return Response.create_failure_response('Could not start Annotation process')


''' Dataset Creation ===> upload file API'''


@app.route('/sphere/api/v1/datasetCreation', methods=['POST'])
def create_dataset():
	try:
		form = request.form
		dataset_name = ''
		dataset_language = ''
		dataset_description = ''
		dataset_delimiter = ''
		dataset_encoding = ''
		dataset_files = None

		for key, value in form.items():
			if key == 'name':
				dataset_name = value
			if key == 'language':
				dataset_language = value
			if key == 'description':
				dataset_description = value
			if key == 'delimiter':
				dataset_delimiter = value
			if key == 'encoding':
				dataset_encoding = value

		for upload in request.files.getlist('file'):
			dataset_files = upload

		document_parser = DocumentParser(dataset_name, document_language=dataset_language,
		                                 document_delimiter=dataset_delimiter,
		                                 document_encoding=dataset_encoding,
		                                 documents='', document_description=dataset_description)

		valid, error = document_parser.check_document_validity()

		name, extension = os.path.splitext(dataset_files.filename)
		PrintHelper.info_print("extention   :", extension)
		file_name = document_parser.save_document(dataset_files, extension)

		if valid and file_name:
			PrintHelper.info_print(error)
			start_background_document_parsing.delay(dataset_name,
			                                        document_language=dataset_language,
			                                        document_delimiter=dataset_delimiter,
			                                        document_encoding=dataset_encoding,
			                                        documents=file_name,
			                                        document_description=dataset_description)

			return Response.create_success_response("Succcess", "Success")
		else:
			PrintHelper.failure_print(error)
			return Response.create_failure_response(error)

	except Exception as e:
		PrintHelper.failure_print(e)
		return Response.create_failure_response("An Error Occured:  " + str(e))


''' Dataset Creation ++ Search ===> Get all available datasets'''


@app.route('/sphere/api/v1/getTextDatasets', methods=['GET'])
def get_all_datasets():
	try:
		# to update the folders and description into the database
		DocumentParser('', '', '', '', documents='filename.txt', document_description='')
		mongo_results = DatabaseFunctions().find_all_items(gsc.database, gsc.collection)
		if mongo_results is None:
			return Response.create_failure_response("No Gold Standards found. Add one to start")
		else:
			return Response.create_success_response(mongo_results,
			                                        "Successfully fetched Gold Standards")

	except Exception as e:
		return Response.create_failure_response("An error occurred: " + str(e))


''' Dataset Creation ++ Search ===> Download a dataset file'''


@app.route('/sphere/api/v1/downloadFile/<name>/<file_name>/<file_format>', methods=['GET'])
def download_file(name, file_name, file_format):
	for file in os.listdir(os.path.dirname(os.path.realpath(__file__))):
		if file.endswith(".tar.bz2"):
			os.remove(file)

	if file_format == 'zip':
		compressed_name = name + '.tar.bz2'
		with tarfile.open(compressed_name, "w:bz2") as compressed_file:
			compressed_file.add(gold_standards_dir + '/' + name, arcname=compressed_name)

		return send_file(os.path.dirname(os.path.realpath(__file__)) + '/' + compressed_name)
	else:
		return send_file(gold_standards_dir + '/' + name + '/' + file_name)


@app.route('/sphere/api/v1/saveAnnotatedDataset', methods=['POST'])
def save_annotated_dataset():
	if not request.json or 'text' not in request.json or 'name' not in request.json or 'language' not in request.json or 'description' not in request.json:
		abort(400)
	try:
		dataset_name = request.json['name']
		dataset_text = request.json['text']
		dataset_language = request.json['language']

		dataset_description = request.json['description']
		dataset_delimiter = ''
		dataset_encoding = 'utf8'
		document_parser = DocumentParser(dataset_name, document_language=dataset_language,
		                                 document_delimiter=dataset_delimiter, document_encoding=dataset_encoding,
		                                 documents='', document_description=dataset_description,
		                                 document_text_array=dataset_text)
		valid, error = document_parser.check_document_validity()

		if valid:
			document_parser.parse_document()
			return Response.create_success_response("Succcess", "Success")
		else:
			PrintHelper.failure_print(error)
			return Response.create_failure_response(error)

	except Exception as e:
		PrintHelper.failure_print(e)
		return Response.create_failure_response("An Error Occured:  " + str(e))


@app.route('/sphere/api/v1/fileDownload', methods=['POST'])
def download_files():
	type = request.json['type']
	path = request.json['path']
	if type == 'gold':
		return send_file(os.path.dirname(os.path.realpath(__file__)) + path)
	else:
		return send_file(os.path.dirname(os.path.realpath(__file__)) + "/libraries/image_classification" + path)


@app.route('/sphere/api/v1/addLexicon', methods=['POST'])
def save_lexicons():
	if not request.json or 'lexicon' not in request.json or 'name' not in request.json or 'language' \
			not in request.json or 'keys' not in request.json:
		abort(400)

	try:
		lexicons = request.json['lexicon']
		keys = request.json['keys']
		name = request.json['name']
		file_name = "uploaded_" + name
		language = request.json['language']

		mongo_items = {
			LexiconsConstants.lc_name     : name,
			LexiconsConstants.lc_file_name: file_name,
			LexiconsConstants.lc_user_keys: keys,
			LexiconsConstants.lc_language : language,
			LexiconsConstants.lc_lexicons : lexicons
		}

		DatabaseFunctions().insert_item(LexiconsConstants.database, LexiconsConstants.collection, mongo_items)
		save_file_name = UPLOADED_RESOURCES + "/" + name + ".json"
		with open(save_file_name, 'w', encoding="utf8") as file:
			json.dump(lexicons, file)
		return Response.create_success_response("Successfully saved", "Successfully saved")

	except Exception as e:
		PrintHelper().failure_print(e)
		return Response.create_failure_response('Unable to save lexicons file with error:  ' + str(e))


@app.route('/sphere/api/v1/lex', methods=['GET'])
def get_saved_mlexs():
	"""

	:return:
	"""
	if request.method != 'GET':
		abort(403)

	try:
		saved_models = DatabaseFunctions().find_all_items(database_name=LexiconsConstants.database,
		                                                  collection_name=LexiconsConstants.collection)

		if saved_models is None:
			PrintHelper().failure_print('No models found in the database')

			return Response.create_failure_response('No saved models found')

		else:
			return Response.create_success_response(saved_models, 'Fetched models successfully')
	except Exception as e:

		PrintHelper().failure_print('sphere_WebAPI', e)
		return Response.create_failure_response('Database error')


'''WOGE Code'''
database = peewee.MySQLDatabase("uby", host="localhost", port=3306,
                                user="root",
                                passwd=MY_SQL_SECRET_PW)
resource_wrapper = ResourceWrapper(database)


@app.before_request
def before_request_handler():
	try:
		database.connect()
	except peewee.OperationalError:
		traceback.print_exc()


@app.after_request
def after_request_handler(response):
	database.close()
	return response


class WorkFlowList(Resource):
	"""
		GET: Shows all workflows or searches for specific workflow
		POST: Creates a new workflow
	"""

	@staticmethod
	def get():
		if len(request.args) != 0:
			workflows = resource_wrapper.get_workflows(request.args["searchstring"])
		else:
			workflows = resource_wrapper.get_workflows()

		return jsonify(
				{
					Constants.status : Constants.true, Constants.data: workflows,
					Constants.message: 'Workflows'
				})

	@staticmethod
	def post():
		if request.json:
			workflow_data = request.get_json()
			start_background_workflow_creation.delay(workflow_data)

			return Response.create_success_response('', 'Classifier Training')
		else:
			return Response.create_failure_response('No JSON data')


class ClassifierList(Resource):
	"""
		GET: returns all possible
	"""

	@staticmethod
	def get():
		classifier_types = resource_wrapper.get_list_of_resources_for_type("classifier")
		return jsonify({
			Constants.status : Constants.true, Constants.data: classifier_types,
			Constants.message: 'Classifier Types'
		})


class FeatureExtractorList(Resource):
	"""
		GET: returns all possible feature extractors
	"""

	@staticmethod
	def get():
		feature_extraction_types = resource_wrapper.get_list_of_resources_for_type(
				"feature_extraction")
		return jsonify({
			Constants.status : Constants.true, Constants.data: feature_extraction_types,
			Constants.message: 'Feature Extractoion Types'
		})


class GoldStandardList(Resource):
	"""
		GET: returns all possible gold standards
	"""

	@staticmethod
	def get():
		gold_standard_types = resource_wrapper.get_list_of_resources_for_type("gold_standard")
		return jsonify({
			Constants.status : Constants.true, Constants.data: gold_standard_types,
			Constants.message: 'Gold Standard Types'
		})


class ResourceList(Resource):
	"""
		GET: returns all possible resources
	"""

	@staticmethod
	def get():
		resources = resource_wrapper.get_list_of_resources_for_type()
		return jsonify({
			Constants.status : Constants.true, Constants.data: resources,
			Constants.message: 'resources'
		})


class PredictionApi(Resource):
	@staticmethod
	def get(id_workflow):
		if len(request.args) != 0:
			sample = request.args["text"]

			keras_prediction = True
			try:
				path = os.path.join(CLASSIFIERS_DIR_KERAS, id_workflow + ".h5")
				model = load_model(path)
			except FileNotFoundError:
				keras_prediction = False
				pass

			path = os.path.join(CLASSIFIERS_DIR_SK, id_workflow + ".pkl")
			pipeline = dill.load(file=open(path, "rb"))
			if not keras_prediction:
				predictions = pipeline.make_prediction([sample.split()])
			elif keras_prediction:
				pipeline.reconstruct_keras_pipeline(model)
				predictions = pipeline.make_prediction([sample.split()])

			return jsonify({
				Constants.status : Constants.true, Constants.data: predictions[0],
				Constants.message: 'Predicition'
			})


class DBReset(Resource):
	@staticmethod
	def get():
		resource_wrapper.reset_databases()

		return jsonify({Constants.status: Constants.true, Constants.message: 'db reset'})


"""

Celery tasks

"""


@celery.task
def background_image_downloader(image_list):
	"""

	:param image_list:
	:return:
	"""
	ImageDownloader().download_main_image_dataset(image_list)


@celery.task
def start_background_training(selected_model_name, hash_digest, number_of_classes,
                              number_of_samples):
	training_object = Training(selected_model_name, hash_digest, number_of_classes,
	                           number_of_samples)
	training_object.start_model_training()


@celery.task
def start_background_validation(model_name, hash_key):
	"""

	:param model_name:
	:param hash_key:
	:return:
	"""
	ModelValidation().start_validation(model_name, hash_key)


@celery.task
def start_background_document_parsing(dataset_name, document_language,
                                      document_delimiter, document_encoding,
                                      documents, document_description):
	document_parser = DocumentParser(dataset_name, document_language=document_language,
	                                 document_delimiter=document_delimiter,
	                                 document_encoding=document_encoding,
	                                 documents=documents, document_description=document_description)
	document_parser.parse_document()


@celery.task
def start_background_workflow_creation(workflow_data):
	current_classifier = GenericMLclassifierCreator(workflow_data=workflow_data,
	                                                resource_wrapper=resource_wrapper)
	current_classifier.start_building_classifier()

	pass


"""
Error Handlers

"""


@app.errorhandler(404)
def not_found(error):
	"""

	:return:
	"""
	PrintHelper().failure_print('404 Error')
	return make_response(
			jsonify({Constants.status: Constants.false, Constants.error_message: 'Not found'}), 404)


@app.errorhandler(400)
def invalid_data(error):
	"""


	:return:
	"""
	PrintHelper().failure_print('400 Error')
	return make_response(
			jsonify({Constants.status: Constants.false, Constants.error_message: 'Invalid data'}), 400)


@app.errorhandler(403)
def invalid_request(error):
	"""

	:return:
	"""
	PrintHelper().failure_print('403 Error')
	return make_response(
			jsonify({Constants.status: Constants.false, Constants.error_message: 'Invalid request'}),
			403)


# noinspection PyTypeChecker
api.add_resource(WorkFlowList, "/sphere/api/v1/resources/workflows")
# noinspection PyTypeChecker
api.add_resource(ClassifierList, "/sphere/api/v1/resources/classifiers")
# noinspection PyTypeChecker
api.add_resource(FeatureExtractorList, "/sphere/api/v1/resources/extractors")
# noinspection PyTypeChecker
api.add_resource(GoldStandardList, "/sphere/api/v1/resources/goldstandards")
# noinspection PyTypeChecker
api.add_resource(ResourceList, "/sphere/api/v1/resources")
# noinspection PyTypeChecker
api.add_resource(PredictionApi, "/sphere/api/v1/workflow/<id_workflow>/predict")
# noinspection PyTypeChecker
api.add_resource(DBReset, "/api/db_reset")


@app.route('/sphere/api/v1/dbreset/<db_name>', methods=['GET'])
def reset_db(db_name):
	database_name = gsc.database
	if db_name == "gold":
		database_name = gsc.database
	elif db_name == "image":
		database_name = imageDatasetConstants.database
	elif db_name == "model":
		database_name = modelConstants.database

	DatabaseFunctions().delete_database(database_name)
	return Response.create_success_response("success", "success")


if __name__ == '__main__':
	app.run(host='0.0.0.0', port=5000, debug=True)
